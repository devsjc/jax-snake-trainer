import jax.numpy as jnp
import jax.random


def random_layer_params(m: int, n: int, key: jax.random.PRNGKey, scale: float = 1e-2) -> jnp.ndarray:
    """
    A helper function to randomly initialise weights and biases
    for a dense neural network layer

    :param m:
    :param n:
    :param key:
    :param scale:
    :return:
    """


