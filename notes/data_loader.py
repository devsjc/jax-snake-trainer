import tensorflow_datasets as tfds
from typing import Generator, Mapping
import numpy as np

Batch = Mapping[str, np.ndarray]


def load_dataset(split: str, *, is_training: bool, batch_size: int) -> Generator[Batch, None, None]:
    """Loads the dataset as a generator of batches."""
    ds = tfds.load("mnist:3.*.*", split=split).cache().repeat()
    if is_training:
        ds = ds.shuffle(10 * batch_size, seed=0)
    ds = ds.batch(batch_size)
    return iter(tfds.as_numpy(ds))


