import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
import jax.numpy as jnp
import os

from game import State, Action


class Linear_QNet(nn.Module):
    def __init__(self, input_size: int, hidden_size: int, output_size: int):
        super().__init__()
        self.linear1 = nn.Linear(input_size, hidden_size)
        self.Linear2 = nn.Linear(hidden_size, output_size)

    def forward(self, x: jnp.ndarray):
        x = F.relu(self.linear1(x))
        x = self.linear2(x)
        return x

    def save(self, file_name='torch_model.pth'):
        model_folder_path = './model'
        if not os.exists(model_folder_path):
            os.makedirs(model_folder_path)
        file_name = os.path.join(model_folder_path, file_name)
        torch.save(self.state_dict(), file_name)


class QTrainer:
    def __init__(self, model, lr: float, gamma: float):
        self.lr = lr
        self.gamma = gamma
        self.model = model
        self.optimiser = optim.Adam(model.parameters(), lr=self.lr)
        self.criterion = nn.MSELoss()

    def train_step(self, state: State, action: Action, reward: int, next_state: State, done: bool):
        state = torch.tensor(state.to_array(), dtype=torch.float)
        next_state = torch.tensor(next_state.to_array(), dtype=torch.float)
        action = torch.tensor(action.to_array(), dtype=torch.float)
        reward = torch.tensor(reward, dtype=torch.float)

        if len(state.shape) == 1:
            # (1, x)
            state = torch.unsqueeze(state, 0)
            next_state = torch.unsqueeze(next_state, 0)
            action = torch.unsqueeze(action, 0)
            reward = torch.unsqueeze(reward, 0)
            done = (done,)  # convert done to a tuple

        # carry out bellman equations
        # 1: predicted Q values with current state
        pred = self.model(state)
        target = pred.clone()

        # 2: reward + gamma * max(next_predicted Q value) -> only do this if not done

        for idx in range(len(done)):
            Q_new = reward[idx]
            if not done[idx]:
                Q_new = reward[idx] + self.gamma * torch.max(self.model(next_state[idx]))

            target[idx][torch.argmax(action).item()] = Q_new
            self.optimiser.zero_grad()
            loss = self.criterion(target, pred)
            loss.backward()  # backprop
            self.optimiser.step()
